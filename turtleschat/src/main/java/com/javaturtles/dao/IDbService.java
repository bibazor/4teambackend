package com.javaturtles.dao;

import com.javaturtles.entity.ChatMessage;
import com.javaturtles.model.User;
import java.util.Date;
import java.util.List;

public interface IDbService {

    boolean addUser(User user);

    boolean isNotUniqueLogin(String login);

    boolean isNotUniqueEmail(String email);

    boolean confirmUserByLogin(String login, String password);

    boolean confirmUserByEmail(String email, String password);

    boolean setLinkAndNewPassword(String login, String link, String newPassword);

    boolean updatePassword(String link);

    List<String> getUsersList();

    List<ChatMessage> getAllChatHistory();

    List<ChatMessage> getPrivateChatHistory(String login1, String login2);

    boolean addAllMessage(String login, Date date, String message);

    boolean addPrivateMessage(String login, String recipient, Date date, String message);
}
