package com.javaturtles.websocket;

import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import com.javaturtles.cache.UserSessionCache;
import com.javaturtles.context.config.ServerConfiguration;
import com.javaturtles.dto.UserMessage;
import com.javaturtles.exception.NoSuchTopicException;
import com.javaturtles.factory.MessageHandlerFactory;
import com.javaturtles.util.message_handler.MessageHandler;
import com.javaturtles.util.session_handler.ChatSessionsHandler;
import org.apache.log4j.Logger;
import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.annotations.*;
import org.eclipse.jetty.websocket.server.WebSocketHandler;
import org.eclipse.jetty.websocket.servlet.WebSocketServletFactory;
import java.net.URI;
import static com.javaturtles.constants.ChatWebsocketConstants.*;

@WebSocket()
public class ChatWebsocket extends WebSocketHandler {
    private static final Logger log = Logger.getLogger(ChatWebsocket.class);
    private static ChatSessionsHandler chatSessionsHandlerInstance = ServerConfiguration.getChatSessionsHandler();
    private static UserSessionCache userSessionCacheInstance = ServerConfiguration.getUserSessionCache();
    private static MessageHandlerFactory messageHandlerFactoryInstance = ServerConfiguration.getMessageHandlerFactory();
    private static Gson gsonInstance = ServerConfiguration.getGson();
    private String login;

    @OnWebSocketConnect
    public void onConnect(Session session) {
        setSession(session);
        chatSessionsHandlerInstance.notifyUserConnected(login);
        chatSessionsHandlerInstance.sendUsersList(login);
        chatSessionsHandlerInstance.sendAllChatHistory(login);
    }

    @OnWebSocketMessage
    public void onTextMessage(String message) {
        UserMessage userMessage;
        try {
            userMessage = gsonInstance.fromJson(message, UserMessage.class);
        } catch (JsonParseException e) {
            log.error(e.getMessage());
            return;
        }
        MessageHandler messageHandler;
        try {
            messageHandler = messageHandlerFactoryInstance.getUserMessageHandler(userMessage.getTopic());
        } catch (NoSuchTopicException e) {
            log.error(e.getMessage());
            return;
        }
        messageHandler.process(userMessage.getPayload(), login);
    }

    @OnWebSocketError
    public void onError(Throwable e) {
        e.printStackTrace();
        log.error(e.getMessage());
    }

    @OnWebSocketClose
    public void onClose(int statusCode, String reason) {
        chatSessionsHandlerInstance.notifyUserDisconnected(login);
        userSessionCacheInstance.removeSession(login);
    }

    @Override
    public void configure(WebSocketServletFactory webSocketServletFactory) {
        webSocketServletFactory.register(ChatWebsocket.class);
    }

    private void setSession(Session session) {
        session.setIdleTimeout(Long.MAX_VALUE);
        URI sessionUri = session.getUpgradeRequest().getRequestURI();
        try {
            login = sessionUri.getQuery().replaceFirst(LOGIN, EMPTY_FIELD);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        userSessionCacheInstance.addSession(login, session);
    }
}
