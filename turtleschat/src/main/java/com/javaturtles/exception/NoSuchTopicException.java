package com.javaturtles.exception;

public class NoSuchTopicException extends Exception {
    public NoSuchTopicException(String message) {
        super(message);
    }
}
