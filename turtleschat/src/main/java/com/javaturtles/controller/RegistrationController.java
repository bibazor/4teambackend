package com.javaturtles.controller;

import com.javaturtles.dto.ResponseDto;
import com.javaturtles.service.RegistrationService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.stream.Collectors;

public class RegistrationController extends HttpServlet {
    private RegistrationService registrationService;

    public RegistrationController(RegistrationService registrationService) {
        this.registrationService = registrationService;
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String body = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
        ResponseDto responseObject = registrationService.registerUser(body);
        response.getWriter().write(responseObject.getMessage());
        response.setStatus(responseObject.getStatus());
    }
}

