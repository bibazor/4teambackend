package com.javaturtles.filter;


import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class CORSFilter implements Filter {
    private static final Logger log = Logger.getLogger(CORSFilter.class);
    private final String accessOrigin = "Access-Control-Allow-Origin";
    private final String accessAllowMethod = "Access-Control-Allow-Method";
    private final String postMethod = "POST";

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletResponse httpResponse = ((HttpServletResponse) servletResponse);
        HttpServletRequest httpRequest = ((HttpServletRequest) servletRequest);
        httpResponse.addHeader(accessOrigin, "*");
        httpResponse.addHeader(accessAllowMethod, postMethod);
        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {

    }
}
