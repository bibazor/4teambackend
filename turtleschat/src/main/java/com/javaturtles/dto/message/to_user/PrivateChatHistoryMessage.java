package com.javaturtles.dto.message.to_user;

import com.javaturtles.entity.ChatMessage;

import java.util.List;
import java.util.Objects;

public class PrivateChatHistoryMessage {
    private final String user;
    private final String receiver;
    private final List<ChatMessage> chatMessages;

    public PrivateChatHistoryMessage(String user, String receiver, List<ChatMessage> chatMessages) {
        this.user = user;
        this.receiver = receiver;
        this.chatMessages = chatMessages;
    }

    public String getUser() {
        return user;
    }

    public String getReceiver() {
        return receiver;
    }

    public List<ChatMessage> getChatMessages() {
        return chatMessages;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PrivateChatHistoryMessage that = (PrivateChatHistoryMessage) o;
        return Objects.equals(user, that.user) && Objects.equals(receiver, that.receiver) && Objects.equals(chatMessages, that.chatMessages);
    }

    @Override
    public int hashCode() {
        return Objects.hash(user, receiver, chatMessages);
    }

    @Override
    public String toString() {
        return "PrivateChatHistoryMessage{" +
                "user='" + user + '\'' +
                ", interlocutor='" + receiver + '\'' +
                ", chatMessages=" + chatMessages +
                '}';
    }
}
