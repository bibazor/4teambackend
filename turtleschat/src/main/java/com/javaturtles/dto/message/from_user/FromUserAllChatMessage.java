package com.javaturtles.dto.message.from_user;

import java.io.Serializable;
import java.util.Objects;

public class FromUserAllChatMessage implements Serializable {
    private final String userLogin;
    private final String message;

    public FromUserAllChatMessage(String userLogin, String message) {
        this.userLogin = userLogin;
        this.message = message;
    }

    public String getUserLogin() {
        return userLogin;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FromUserAllChatMessage that = (FromUserAllChatMessage) o;
        return Objects.equals(userLogin, that.userLogin) && Objects.equals(message, that.message);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userLogin, message);
    }

    @Override
    public String toString() {
        return "FromUserAllChatMessage{" +
                "userLogin='" + userLogin + '\'' +
                ", message='" + message + '\'' +
                '}';
    }
}
