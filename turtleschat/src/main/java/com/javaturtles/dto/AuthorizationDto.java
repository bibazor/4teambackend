package com.javaturtles.dto;

import java.io.Serializable;

public class AuthorizationDto implements Serializable {
    private final String login;
    private final String password;

    public AuthorizationDto(String login, String password) {
        this.login = login;
        this.password = password;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public String toString() {
        return "LoginDTO{" +
                "login='" + login + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
