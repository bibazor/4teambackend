package com.javaturtles.util;

import com.google.gson.Gson;
import com.javaturtles.dto.UserMessage;

public class MessageConverter {
    private Gson gson;

    public MessageConverter(Gson gson) {
        this.gson = gson;
    }

    public String createToUserMessage(String action, String message) {
        UserMessage toUserMessage = new UserMessage(action, message);
        String toUserMessageString = gson.toJson(toUserMessage);
        return toUserMessageString;
    }

    public String createToUserMessage(String action, Object message) {
        String disconnectionMessageString = gson.toJson(message);
        UserMessage toUserMessage = new UserMessage(action, disconnectionMessageString);
        String toUserMessageString = gson.toJson(toUserMessage);
        return toUserMessageString;
    }
}
