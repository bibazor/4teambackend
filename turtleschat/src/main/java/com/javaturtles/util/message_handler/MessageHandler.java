package com.javaturtles.util.message_handler;

import com.javaturtles.websocket.ChatWebsocket;
import org.eclipse.jetty.websocket.api.Session;

public interface MessageHandler {
    String getMessageRequest();

    void process(String message, String login);
}
