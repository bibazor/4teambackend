package com.javaturtles.util.message_handler;

import com.google.gson.Gson;
import com.javaturtles.dto.message.from_user.FromUserPrivateChatMessage;
import com.javaturtles.util.session_handler.ChatSessionsHandler;
import com.javaturtles.util.validation.Validation;
import com.javaturtles.websocket.ChatWebsocket;
import org.apache.log4j.Logger;

import static com.javaturtles.constants.FromUserTopicConstants.PRIVATE_CHAT_MESSAGE_FROM_USER;
import static com.javaturtles.constants.ResponseMessage.MSG_SUCCESS;

public class FromPrivateChatMessageHandler implements MessageHandler {
    private static final Logger log = Logger.getLogger(ChatWebsocket.class);
    private ChatSessionsHandler chatSessionsHandler;
    private Validation validation;
    private Gson gson;

    public FromPrivateChatMessageHandler(ChatSessionsHandler chatSessionsHandler, Validation validation, Gson gson) {
        this.chatSessionsHandler = chatSessionsHandler;
        this.validation = validation;
        this.gson = gson;
    }

    @Override
    public String getMessageRequest() {
        return PRIVATE_CHAT_MESSAGE_FROM_USER;
    }

    @Override
    public void process(String message, String login) {
        FromUserPrivateChatMessage fromUserPrivateChatMessage = gson.fromJson(message, FromUserPrivateChatMessage.class);
        String validationResult = validation.getValidateResponse(fromUserPrivateChatMessage);
        if (!validationResult.equals(MSG_SUCCESS)) {
            log.error(validationResult);
            return;
        }
        System.out.println(fromUserPrivateChatMessage);
        chatSessionsHandler.sendPrivateChatMessage(fromUserPrivateChatMessage);
    }
}
