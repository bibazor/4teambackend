package com.javaturtles.util;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class PropertyFileService {
    private static final String PROPERTY_PATH = "src/main/resources/config.properties";
    private String host;
    private String login;
    private String password;

    public String getDBHostFromProperty() {
        setFromProperty();
        return host;
    }

    public String getDBLoginFromProperty() {
        setFromProperty();
        return login;
    }

    public String getDBPasswordFromProperty() {
        setFromProperty();
        return password;
    }

    private void setFromProperty() {

        Properties property = new Properties();
        try (FileInputStream fis = new FileInputStream(PROPERTY_PATH)) {
            property.load(fis);
            host = property.getProperty("db.host");
            login = property.getProperty("db.login");
            password = property.getProperty("db.password");
        } catch (IOException e) {
            host = "";
            login = "";
            password = "";
        }
    }
}
