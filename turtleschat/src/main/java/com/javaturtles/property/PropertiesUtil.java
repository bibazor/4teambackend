package com.javaturtles.property;

import org.apache.log4j.Logger;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import static com.javaturtles.constants.PropertiesConstants.*;

public class PropertiesUtil {
    private static final Logger log = Logger.getLogger(PropertiesUtil.class);
    private static volatile PropertiesUtil uniqueInstance;
    private Properties properties;
    private String jwtSecretKey;
    private String salt;
    private String email;
    private String mailPassword;

    private PropertiesUtil() {
        configure();
    }

    public static PropertiesUtil getInstance() {
        if (uniqueInstance == null) {
            synchronized (PropertiesUtil.class) {
                if (uniqueInstance == null) {
                    uniqueInstance = new PropertiesUtil();
                }
            }
        }
        return uniqueInstance;
    }

    public String getJwtSecretKey() {
        return jwtSecretKey;
    }

    public String getSalt(){
        return salt;
    }

    public String getEmail() {
        return email;
    }

    public String getMailPassword() {
        return mailPassword;
    }

    private void configure() {
        properties = new Properties();
        setProperty();
    }

    private void setProperty() {

        try (InputStream inputStream = new FileInputStream(CONFIG_PROPERTIES_PATH)) {
            properties.load(inputStream);
            String jwtSecretKey = properties.getProperty(JWT_PROPERTY);
            String salt =  properties.getProperty(SALT_PROPERTY);
            String email = properties.getProperty(EMAIL);
            String mailPassword = properties.getProperty(MAIL_PASSWORD);
            this.jwtSecretKey = jwtSecretKey;
            this.salt = salt;
            this.email = email;
            this.mailPassword = mailPassword;
        } catch (IOException e) {
            log.error(e.getMessage());
        }
    }
}
