package com.javaturtles.constants;

public class FromUserTopicConstants {
    public static final String ALL_CHAT_HISTORY_REQUEST = "all_chat_history_request"; // пейлоуд пустая строка
    public static final String PRIVATE_CHAT_HISTORY_REQUEST = "private_chat_history_request"; //пэйлоуд - логин
    public static final String ALL_CHAT_MESSAGE_FROM_USER = "all_chat_message"; // объект
    public static final String PRIVATE_CHAT_MESSAGE_FROM_USER = "private_chat_message"; // объект
}
