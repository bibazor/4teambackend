package com.javaturtles.constants;

public class ConfigurationConstants {
    public static final String HOST = "localhost";
    public static final int PORT = 8091;
}
