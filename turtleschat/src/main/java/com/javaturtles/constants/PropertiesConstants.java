package com.javaturtles.constants;

public class PropertiesConstants {
    public static final String SALT_PROPERTY = "salt";
    public static final String SALT_PATH = "src/main/resources/salt.properties";
    public static final String PROPERTIES_PATH = "src/main/resources/config.properties";
    public static final String SESSION_LIFE_TIME_PROPERTY = "session_life_time_millis";
    public static final String EMAIL = "email";
    public static final String MAIL_PASSWORD = "password";
    public static final String CONFIG_PROPERTIES_PATH = "src/main/resources/config.properties";
    public static final String JWT_PROPERTY = "jwt_secret_key";

    private PropertiesConstants() {
    }
}
