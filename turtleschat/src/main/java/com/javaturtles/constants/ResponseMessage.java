package com.javaturtles.constants;

public class ResponseMessage {
    public static final String MSG_SUCCESS = "OK";
    public static final String MSG_CREATED = "CREATED";
    public static final String MSG_INCORRECT_DATA = "Incorrect data";
    public static final String MSG_INVALID_EMPTY_NULL_FIELD = "Fields cannot be null or empty";
    public static final String MSG_INVALID_NULL = "Fields cannot be null";
    public static final String MSG_INVALID_INCORRECT_LOGIN = "Incorrect login";
    public static final String MSG_INVALID_INCORRECT_PASSWORD = "Incorrect password";
    public static final String MSG_INVALID_CONFIRM_PASSWORD_MISMATCH = "Password mismatch";
    public static final String MSG_INVALID_INCORRECT_EMAIL = "Incorrect e-mail";
    public static final String MSG_INVALID_INCORRECT_NUMBER = "Incorrect number";
    public static final String MSG_INVALID_EXCEEDED_CHARACTERS = "Exceeded number of characters";
    public static final String MSG_INVALID_CREDENTIALS = "Invalid credentials";
    public static final String MSG_CANNOT_SEND = "Cannot send message";
    public static final String MSG_NOT_POSSIBLE_RECOVER = "Recover not possible";
    public static final String MSG_SUCCESS_PASSWORD_RECOVER = "The password has been changed";

    private ResponseMessage() {
    }
}
