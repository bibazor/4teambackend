package com.javaturtles.testdata.service;

import com.javaturtles.dto.AuthorizationDto;
import com.javaturtles.dto.ResponseDto;
import javax.servlet.http.HttpServletResponse;

public class AuthorizationServiceTestData {
    public static final String JWT_TOKEN = "QWERTY1234567890ZXCVBBNMGUKIV$UCJCDTJ";
    public static final int TT_MILLIS = 4;
    public static final String MSG_SUCCESS = "OK";
    public static final String MSG_INVALID_INCORRECT_LOGIN = "Incorrect login";
    public static final String MSG_INCORRECT_DATA = "Incorrect data";
    public static final String BODY_1 = "{\"login\":\"loginUser\",\"password\":\"Password007\"}";
    public static final String BODY_2 = "{\"login\":\"badlog\",\"password\":\"Password007\"}";
    public static final String BODY_3 = "not serializable string";
    public static final AuthorizationDto AUTH_DTO_1_SUCCESS = new AuthorizationDto("loginUser", "Password007s");
    public static final AuthorizationDto AUTH_DTO_2_WRONG = new AuthorizationDto("bad log", "Password007");
    public static final ResponseDto RESPONSE_DTO_SUCCESS = new ResponseDto(JWT_TOKEN, HttpServletResponse.SC_OK);
    public static final ResponseDto RESPONSE_DTO_INVALID_LOGIN = new ResponseDto(MSG_INVALID_INCORRECT_LOGIN, HttpServletResponse.SC_UNAUTHORIZED);
    public static final ResponseDto RESPONSE_DTO_INCORRECT_DATA = new ResponseDto(MSG_INCORRECT_DATA, HttpServletResponse.SC_UNAUTHORIZED);

}
