package com.javaturtles.testdata;

import com.google.gson.Gson;
import com.javaturtles.dto.UserMessage;
import com.javaturtles.dto.message.to_user.PrivateChatHistoryMessage;
import com.javaturtles.dto.message.to_user.ToUserChatMessage;
import com.javaturtles.entity.ChatMessage;
import java.util.*;
import static com.javaturtles.constants.ToUserTopicConstants.*;
import static com.javaturtles.constants.UserStatusConstants.*;

public class MessageConverterTestData {
    public static final String USERS_LIST_ACTION = USERS_LIST;
    public static final String ALL_CHAT_MESSAGE_TO_USER_ACTION = ALL_CHAT_MESSAGE_TO_USER;
    public static final String PRIVATE_CHAT_MESSAGE_TO_USER_ACTION = PRIVATE_CHAT_MESSAGE_TO_USER;
    public static final String ALL_CHAT_HISTORY_ACTION = ALL_CHAT_HISTORY;
    public static final String PRIVATE_CHAT_HISTORY_ACTION = PRIVATE_CHAT_HISTORY;
    public static final String USER_CONNECTION_ACTION = USER_CONNECTION;
    public static final String USER_DISCONNECTION_ACTION = USER_DISCONNECTION;
    public static final String USER_LOGIN = "userLogin";
    public static final String USER_LOGIN_1 = "userLogin1";
    public static final String USER_LOGIN_2 = "userLogin2";
    public static final String RECEIVER_LOGIN = "receiverLogin";
    public static final String MESSAGE = "Chat message";
    public static final String MESSAGE_1 = "Chat message 1";
    public static final String MESSAGE_2 = "Chat message 2";
    public static final Date DATE_1 = new Date(1_000_000L);
    public static final Date DATE_2 = new Date(2_000_000L);
    public static final ChatMessage CHAT_MESSAGE = new ChatMessage(USER_LOGIN_1, DATE_1, MESSAGE_1);
    public static final ChatMessage CHAT_MESSAGE_2 = new ChatMessage(USER_LOGIN_2, DATE_2, MESSAGE_2);
    public static final List<ChatMessage> CHAT_MESSAGES = new ArrayList<>(List.of(CHAT_MESSAGE, CHAT_MESSAGE_2));
    public static final PrivateChatHistoryMessage PRIVATE_CHAT_HISTORY_MESSAGE =
            new PrivateChatHistoryMessage(USER_LOGIN, RECEIVER_LOGIN, CHAT_MESSAGES);
    public static final ToUserChatMessage ALL_CHAT_MESSAGE = new ToUserChatMessage(USER_LOGIN, null, DATE_1,
            MESSAGE_1);
    public static final ToUserChatMessage PRIVATE_CHAT_MESSAGE = new ToUserChatMessage(USER_LOGIN, RECEIVER_LOGIN, DATE_2,
            MESSAGE_2);
    public static final Map<String, String> USERS_MAP = new HashMap<>();
    public static final String USERS_LIST_EXPECTED = "{\"topic\":\"users_list\",\"payload\":\"{\\\"userLogin1\\\":\\\"online\\\",\\\"userLogin2\\\":\\\"offline\\\"}\"}";
    public static final String ALL_CHAT_MESSAGE_TO_USER_EXPECTED = "{\"topic\":\"all_chat_message\",\"payload\":\"{\\\"userLogin\\\":\\\"userLogin\\\",\\\"messageDate\\\":\\\"Jan 1, 1970, 3:16:40 AM\\\",\\\"message\\\":\\\"Chat message 1\\\"}\"}";
    public static final String PRIVATE_CHAT_MESSAGE_TO_USER_EXPECTED = "{\"topic\":\"private_chat_message\",\"payload\":\"{\\\"userLogin\\\":\\\"userLogin\\\",\\\"receiverLogin\\\":\\\"receiverLogin\\\",\\\"messageDate\\\":\\\"Jan 1, 1970, 3:33:20 AM\\\",\\\"message\\\":\\\"Chat message 2\\\"}\"}";
    public static final String ALL_CHAT_HISTORY_EXPECTED = "{\"topic\":\"all_chat_history\",\"payload\":\"[{\\\"login\\\":\\\"userLogin1\\\",\\\"date\\\":\\\"Jan 1, 1970, 3:16:40 AM\\\",\\\"message\\\":\\\"Chat message 1\\\"},{\\\"login\\\":\\\"userLogin2\\\",\\\"date\\\":\\\"Jan 1, 1970, 3:33:20 AM\\\",\\\"message\\\":\\\"Chat message 2\\\"}]\"}";
    public static final String PRIVATE_CHAT_HISTORY_EXPECTED = "{\"topic\":\"private_chat_history\",\"payload\":\"{\\\"user\\\":\\\"userLogin\\\",\\\"receiver\\\":\\\"receiverLogin\\\",\\\"chatMessages\\\":[{\\\"login\\\":\\\"userLogin1\\\",\\\"date\\\":\\\"Jan 1, 1970, 3:16:40 AM\\\",\\\"message\\\":\\\"Chat message 1\\\"},{\\\"login\\\":\\\"userLogin2\\\",\\\"date\\\":\\\"Jan 1, 1970, 3:33:20 AM\\\",\\\"message\\\":\\\"Chat message 2\\\"}]}\"}";
    public static final String USER_CONNECTION_EXPECTED = "{\"topic\":\"user_connection\",\"payload\":\"userLogin\"}";
    public static final String USER_DISCONNECTION_EXPECTED = "{\"topic\":\"user_disconnection\",\"payload\":\"userLogin\"}";

    static {
        USERS_MAP.put(USER_LOGIN_1, ONLINE);
        USERS_MAP.put(USER_LOGIN_2, OFFLINE);
    }
}
