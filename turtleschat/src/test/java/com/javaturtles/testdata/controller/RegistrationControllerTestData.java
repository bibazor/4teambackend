package com.javaturtles.testdata.controller;


public class RegistrationControllerTestData {

    public static final String RESPONSE_INCORRECT_DATA = "Incorrect data";

    private static final String CORRECT_LOGIN_1 = "userLogin";
    private static final String CORRECT_LOGIN_2 = "donatello";
    private static final String INCORRECT_LOGIN_1 = "user_login";

    private static final String CORRECT_PASSWORD_1 = "passworD1";
    private static final String CORRECT_PASSWORD_2 = "Donatello07";
    private static final String INCORRECT_PASSWORD_1 = "passworD";

    private static final String CORRECT_CONFIRM_CORRECT_PASSWORD_1 = "passworD1";
    private static final String CORRECT_CONFIRM_CORRECT_PASSWORD_2 = "Donatello07";

    private static final String INCORRECT_CONFIRM_CORRECT_PASSWORD_1 = "passworDD";
    private static final String CORRECT_CONFIRM_INCORRECT_PASSWORD_1 = "passworD";

    private static final String CORRECT_EMAIL_1 = "email@qwe.com";
    private static final String CORRECT_EMAIL_2 = "e-mail@qwe.ua";
    private static final String INCORRECT_EMAIL_1 = "email.qwe.com";

    private static final String CORRECT_PHONE_NUMBER = "+380669801221";
    private static final String CORRECT_PHONE_NUMBER_2 = "+80066980777";
    private static final String INCORRECT_PHONE_NUMBER_1 = "380669801221";

    private static final String CORRECT_COMPANY_NAME_1 = "";
    private static final String CORRECT_COMPANY_NAME_2 = "Afrodita";
    private static final String INCORRECT_COMPANY_NAME_1 = "12345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901";

    public static String CORRECT_BODY_1 = "{\n" +
            "    \"login\":"+CORRECT_LOGIN_1+",\n" +
            "    \"password\":"+ CORRECT_PASSWORD_1 +",\n" +
            "    \"confirmPassword\":"+ CORRECT_CONFIRM_CORRECT_PASSWORD_1 +",\n" +
            "    \"email\":"+CORRECT_EMAIL_1+",\n" +
            "    \"phoneNumber\":"+CORRECT_PHONE_NUMBER+",\n" +
            "    \"company\":"+CORRECT_COMPANY_NAME_1+"\n" +
            "}";

    public static String CORRECT_BODY_2 = "{\n" +
            "    \"login\":"+CORRECT_LOGIN_2+",\n" +
            "    \"password\":"+ CORRECT_PASSWORD_2 +",\n" +
            "    \"confirmPassword\":"+ CORRECT_CONFIRM_CORRECT_PASSWORD_2 +",\n" +
            "    \"email\":"+CORRECT_EMAIL_2+",\n" +
            "    \"phoneNumber\":"+CORRECT_PHONE_NUMBER_2+",\n" +
            "    \"company\":"+CORRECT_COMPANY_NAME_2+"\n" +
            "}";


    public static final String BODY_INCORRECT_LOGIN = "{\n" +
            "    \"login\":user_login,\n" +
            "    \"password\":passworD1,\n" +
            "    \"confirmPassword\":passworD1,\n" +
            "    \"email\":email@qwe.com,\n" +
            "    \"phoneNumber\":+380669801221,\n" +
            "    \"company\":\n" +
            "}";

    public static final String BODY_INCORRECT_PASSWORD = "{\n" +
            "    \"login\":userLogin,\n" +
            "    \"password\":passworD,\n" +
            "    \"confirmPassword\":passworD,\n" +
            "    \"email\":email@qwe.com,\n" +
            "    \"phoneNumber\":+380669801221,\n" +
            "    \"company\":\n" +
            "}";

    public static final String BODY_INCORRECT_CONFIRM = "{\n" +
            "    \"login\":userLogin,\n" +
            "    \"password\":passworD1,\n" +
            "    \"confirmPassword\":passworDD,\n" +
            "    \"email\":email@qwe.com,\n" +
            "    \"phoneNumber\":+380669801221,\n" +
            "    \"company\":\n" +
            "}";

    public static final String BODY_INCORRECT_EMAIL = "{\n" +
            "    \"login\":userLogin,\n" +
            "    \"password\":passworD1,\n" +
            "    \"confirmPassword\":passworD1,\n" +
            "    \"email\":email.qwe.com,\n" +
            "    \"phoneNumber\":+380669801221,\n" +
            "    \"company\":\n" +
            "}";

    public static final String BODY_INCORRECT_NUMBER = "{\n" +
            "    \"login\":userLogin,\n" +
            "    \"password\":passworD1,\n" +
            "    \"confirmPassword\":passworD1,\n" +
            "    \"email\":email@qwe.com,\n" +
            "    \"phoneNumber\":380669801221,\n" +
            "    \"company\":\n" +
            "}";

    public static final String BODY_INCORRECT_COMPANY = "{\n" +
            "    \"login\":userLogin,\n" +
            "    \"password\":passworD1,\n" +
            "    \"confirmPassword\":passworD1,\n" +
            "    \"email\":email@qwe.com,\n" +
            "    \"phoneNumber\":+380669801221,\n" +
            "    \"company\":12345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901\n" +
            "}";

    public static final String MSG_CREATED = "CREATED";

    public static final String MSG_INVALID_NULL = "Fields cannot be null";


}
