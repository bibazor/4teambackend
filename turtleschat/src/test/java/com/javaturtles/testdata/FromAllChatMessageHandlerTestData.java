package com.javaturtles.testdata;

import com.javaturtles.dto.message.from_user.FromUserAllChatMessage;

public class FromAllChatMessageHandlerTestData {
    public static final String MESSAGE_1 = "{\"userLogin\":\"certainLogin1\",\"message\":\"Certain message from user 1\"}";
    public static final String MESSAGE_2 = "{\"userLogin\":\"certainLogin2\",\"message\":\"Certain message from user 2\"}";
    public static final String CERTAIN_LOGIN_1 = "certainLogin1";
    public static final String CERTAIN_LOGIN_2 = "certainLogin2";
    public static final String CERTAIN_MESSAGE_1 = "Certain message from user 1";
    public static final String CERTAIN_MESSAGE_2 = "Certain message from user 2";
    public static final FromUserAllChatMessage fromUserAllChatMessage1 = new FromUserAllChatMessage(CERTAIN_LOGIN_1,
            CERTAIN_MESSAGE_1);
    public static final FromUserAllChatMessage fromUserAllChatMessage2 = new FromUserAllChatMessage(CERTAIN_LOGIN_2,
            CERTAIN_MESSAGE_2);
    public static final int SEND_TIMES = 1;
}
