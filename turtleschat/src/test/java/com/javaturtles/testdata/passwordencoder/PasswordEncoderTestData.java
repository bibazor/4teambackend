package com.javaturtles.testdata.passwordencoder;

public class PasswordEncoderTestData {

    public static final String PASSWORD_1 = "Password2022";
    public static final String HASHED_PASSWORD_1 = "56e1a7524db07d079d1f5e49a6b3d702d3fe7bcd145fd990f142baf8489369ab";
    public static final String PASSWORD_2 = "30SecondToMars";
    public static final String HASHED_PASSWORD_2 = "0441394a2a01ceaa4ab9de2e47ccdcd503ea4473e221c6ec262d965ff3debd2d";

}
