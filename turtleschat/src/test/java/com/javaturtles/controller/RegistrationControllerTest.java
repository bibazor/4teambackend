package com.javaturtles.controller;

import com.javaturtles.dto.ResponseDto;
import com.javaturtles.service.RegistrationService;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import static com.javaturtles.testdata.controller.RegistrationControllerTestData.*;

class RegistrationControllerTest {

    BufferedReader reader = Mockito.mock(BufferedReader.class);
    Stream<String> stream = Mockito.mock(Stream.class);
    PrintWriter writer = Mockito.mock(PrintWriter.class);
    Collector collector = Mockito.mock(Collector.class);
    HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
    HttpServletResponse response = Mockito.mock(HttpServletResponse.class);

    RegistrationService registrationService = Mockito.mock(RegistrationService.class);
    RegistrationController cut = new RegistrationController(registrationService);

    static Arguments[] doPostTestArgs() {
        return new Arguments[]{

                Arguments.arguments(CORRECT_BODY_1, new ResponseDto(MSG_CREATED, 201), 1, 1),
                Arguments.arguments(CORRECT_BODY_2, new ResponseDto(MSG_CREATED, 201), 1, 1),
                Arguments.arguments(CORRECT_BODY_2, new ResponseDto(MSG_CREATED, 201), 1, 1),
                Arguments.arguments(null, new ResponseDto(MSG_INVALID_NULL, 400), 1, 1),

        };
    }

    @MethodSource("doPostTestArgs")
    @ParameterizedTest
    void doPostTest(String body, ResponseDto responseDto, int times1, int times2) throws IOException, ServletException {

        try (MockedStatic<Collectors> mockedCollectors = Mockito.mockStatic(Collectors.class)) {

            Mockito.when(request.getReader()).thenReturn(reader);
            Mockito.when(reader.lines()).thenReturn(stream);
            mockedCollectors.when(() -> Collectors.joining(System.lineSeparator())).thenReturn(collector);
            Mockito.when(stream.collect(Collectors.joining(System.lineSeparator()))).thenReturn(body);
            Mockito.when(registrationService.registerUser(body)).thenReturn(responseDto);
            Mockito.when(response.getWriter()).thenReturn(writer);

            cut.doPost(request, response);

            Mockito.verify(writer, Mockito.times(times1)).write(responseDto.getMessage());
            Mockito.verify(response, Mockito.times(times2)).setStatus(responseDto.getStatus());
        }
    }
}
