package com.javaturtles.util.message_handler;

import com.javaturtles.util.session_handler.ChatSessionsHandler;
import com.javaturtles.util.validation.Validation;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import static com.javaturtles.constants.FromUserTopicConstants.PRIVATE_CHAT_HISTORY_REQUEST;
import static com.javaturtles.constants.ResponseMessage.MSG_SUCCESS;
import static com.javaturtles.testdata.PrivateChatHistoryRequestHandlerTestData.*;

public class PrivateChatHistoryRequestHandlerTest {

    private static final ChatSessionsHandler chatSessionsHandler = Mockito.mock(ChatSessionsHandler.class);
    private static final Validation validation = Mockito.mock(Validation.class);

    private static final PrivateChatHistoryRequestHandler cut = new PrivateChatHistoryRequestHandler(chatSessionsHandler,
            validation);

    @Test
    void processTest() {
        Mockito.when(validation.getLoginValidateResponse(CERTAIN_LOGIN)).thenReturn(MSG_SUCCESS);

        cut.process(CERTAIN_MESSAGE, CERTAIN_LOGIN);

        Mockito.verify(chatSessionsHandler, Mockito.times(SEND_HISTORY_TIMES)).sendPrivateChatHistory(CERTAIN_LOGIN,
                CERTAIN_MESSAGE);
    }

    @Test
    void getMessageRequestTest() {
        String actual = cut.getMessageRequest();
        String expected = PRIVATE_CHAT_HISTORY_REQUEST;
        Assertions.assertEquals(expected, actual);
    }
}
