package com.javaturtles.util.session_handler;

import com.javaturtles.cache.UserSessionCache;
import com.javaturtles.dao.IDbService;
import com.javaturtles.util.MessageConverter;
import com.javaturtles.util.TimeUtil;
import org.eclipse.jetty.websocket.api.RemoteEndpoint;
import org.eclipse.jetty.websocket.api.Session;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import java.io.IOException;
import static com.javaturtles.constants.ToUserTopicConstants.*;
import static com.javaturtles.testdata.ChatSessionsHandlerTestData.*;

public class ChatSessionsHandlerTest {
    private static final MessageConverter messageConverter = Mockito.mock(MessageConverter.class);
    private static final UserSessionCache userSessionCache = Mockito.mock(UserSessionCache.class);
    private static final IDbService dbService = Mockito.mock(IDbService.class);
    private static final Session session = Mockito.mock(Session.class);
    private static final Session receiverSession = Mockito.mock(Session.class);
    private static final RemoteEndpoint remoteEndpoint = Mockito.mock(RemoteEndpoint.class);
    private static final RemoteEndpoint remoteReceiverEndpoint = Mockito.mock(RemoteEndpoint.class);
    private static final RemoteEndpoint login1RemoteEndpoint = Mockito.mock(RemoteEndpoint.class);
    private static final RemoteEndpoint login3RemoteEndpoint = Mockito.mock(RemoteEndpoint.class);
    private static final Session login1Session = Mockito.mock(Session.class);
    private static final Session login3Session = Mockito.mock(Session.class);

    private static final ChatSessionsHandler cut = new ChatSessionsHandler(messageConverter, userSessionCache,
            dbService);

    @Test
    void sendUsersListTest() throws IOException {
        Mockito.when(userSessionCache.getSession(CERTAIN_LOGIN)).thenReturn(session);
        Mockito.when(dbService.getUsersList()).thenReturn(USER_LIST);
        Mockito.when(userSessionCache.getLoginSet()).thenReturn(ONLINE_USERS);
        Mockito.when(messageConverter.createToUserMessage(USERS_LIST, USERS_MAP)).thenReturn(TO_USER_USERS_LIST_MESSAGE);
        Mockito.when(session.getRemote()).thenReturn(remoteEndpoint);

        cut.sendUsersList(CERTAIN_LOGIN);

        Mockito.verify(remoteEndpoint, Mockito.times(SEND_LIST_TIMES)).sendString(TO_USER_USERS_LIST_MESSAGE);
    }

    @Test
    void sendAllChatMessageTest() {
        try (MockedStatic<TimeUtil> mockedTimeUtil = Mockito.mockStatic(TimeUtil.class)) {
            mockedTimeUtil.when(TimeUtil::getCurrentTimeMillis).thenReturn(NOW_MILLIS);
            Mockito.when(messageConverter.createToUserMessage(ALL_CHAT_MESSAGE_TO_USER, ALL_CHAT_MESSAGE_OBJECT))
                    .thenReturn(TO_USER_ALL_CHAT_MESSAGE);
            Mockito.when(userSessionCache.getLoginSet()).thenReturn(ONLINE_USERS);
            Mockito.when(userSessionCache.getSession(CERTAIN_LOGIN_1)).thenReturn(login1Session);
            Mockito.when(userSessionCache.getSession(CERTAIN_LOGIN_3)).thenReturn(login3Session);
            Mockito.when(login1Session.getRemote()).thenReturn(login1RemoteEndpoint);
            Mockito.when(login3Session.getRemote()).thenReturn(login3RemoteEndpoint);

            cut.sendAllChatMessage(FROM_USER_ALL_CHAT_MESSAGE);

            Mockito.verify(dbService, Mockito.times(1)).addAllMessage(CERTAIN_LOGIN, NOW_DATE, CERTAIN_ALL_CHAT_MESSAGE);
            Mockito.verify(login1RemoteEndpoint, Mockito.times(1))
                    .sendString(TO_USER_ALL_CHAT_MESSAGE);
            Mockito.verify(login3RemoteEndpoint, Mockito.times(1))
                    .sendString(TO_USER_ALL_CHAT_MESSAGE);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    void sendPrivateChatMessageTest() throws IOException {
        try (MockedStatic<TimeUtil> mockedTimeUtil = Mockito.mockStatic(TimeUtil.class)) {
            mockedTimeUtil.when(TimeUtil::getCurrentTimeMillis).thenReturn(NOW_MILLIS);
            Mockito.when(messageConverter.createToUserMessage(PRIVATE_CHAT_MESSAGE_TO_USER,
                    PRIVATE_CHAT_MESSAGE_OBJECT)).thenReturn(TO_USER_PRIVATE_CHAT_MESSAGE);
            Mockito.when(userSessionCache.getSession(CERTAIN_LOGIN)).thenReturn(session);
            Mockito.when(userSessionCache.getSession(RECEIVER_LOGIN)).thenReturn(receiverSession);
            Mockito.when(session.getRemote()).thenReturn(remoteEndpoint);
            Mockito.when(receiverSession.getRemote()).thenReturn(remoteReceiverEndpoint);

            cut.sendPrivateChatMessage(FROM_USER_PRIVATE_CHAT_MESSAGE);

            Mockito.verify(dbService, Mockito.times(1)).addPrivateMessage(CERTAIN_LOGIN, RECEIVER_LOGIN, NOW_DATE,
                    CERTAIN_PRIVATE_CHAT_MESSAGE);
            Mockito.verify(remoteEndpoint, Mockito.times(1))
                    .sendString(TO_USER_PRIVATE_CHAT_MESSAGE);
            Mockito.verify(remoteReceiverEndpoint, Mockito.times(1))
                    .sendString(TO_USER_PRIVATE_CHAT_MESSAGE);
        }
    }

    @Test
    void sendAllChatHistoryTest() throws IOException {
        Mockito.when(userSessionCache.getSession(CERTAIN_LOGIN)).thenReturn(session);
        Mockito.when(dbService.getAllChatHistory()).thenReturn(CHAT_MESSAGE_LIST);
        Mockito.when(messageConverter.createToUserMessage(ALL_CHAT_HISTORY, CHAT_MESSAGE_LIST))
                .thenReturn(TO_USER_ALL_CHAT_HISTORY);
        Mockito.when(session.getRemote()).thenReturn(remoteEndpoint);

        cut.sendAllChatHistory(CERTAIN_LOGIN);

        Mockito.verify(remoteEndpoint, Mockito.times(1))
                .sendString(TO_USER_ALL_CHAT_HISTORY);
    }

    @Test
    void sendPrivateChatHistoryTest() throws IOException {
        Mockito.when(dbService.getPrivateChatHistory(CERTAIN_LOGIN, RECEIVER_LOGIN)).thenReturn(CHAT_MESSAGE_LIST);
        Mockito.when(messageConverter.createToUserMessage(PRIVATE_CHAT_HISTORY, PRIVATE_CHAT_HISTORY_MESSAGE))
                .thenReturn(TO_USER_PRIVATE_CHAT_HISTORY);
        Mockito.when(userSessionCache.getSession(CERTAIN_LOGIN)).thenReturn(session);
        Mockito.when(session.getRemote()).thenReturn(remoteEndpoint);

        cut.sendPrivateChatHistory(CERTAIN_LOGIN, RECEIVER_LOGIN);

        Mockito.verify(remoteEndpoint, Mockito.times(1))
                .sendString(TO_USER_PRIVATE_CHAT_HISTORY);
    }

    @Test
    void notifyUserConnectedTest() throws IOException {
        Mockito.when(messageConverter.createToUserMessage(USER_CONNECTION, CERTAIN_LOGIN))
                .thenReturn(TO_USER_CONNECTION);
        Mockito.when(userSessionCache.getLoginSet()).thenReturn(ONLINE_USERS);
        Mockito.when(userSessionCache.getSession(CERTAIN_LOGIN_1)).thenReturn(login1Session);
        Mockito.when(userSessionCache.getSession(CERTAIN_LOGIN_3)).thenReturn(login3Session);
        Mockito.when(login1Session.getRemote()).thenReturn(login1RemoteEndpoint);
        Mockito.when(login3Session.getRemote()).thenReturn(login3RemoteEndpoint);

        cut.notifyUserConnected(CERTAIN_LOGIN);

        Mockito.verify(login1RemoteEndpoint, Mockito.times(1)).sendString(TO_USER_CONNECTION);
        Mockito.verify(login3RemoteEndpoint, Mockito.times(1)).sendString(TO_USER_CONNECTION);
    }

    @Test
    void notifyUserDisconnected() throws IOException {
        Mockito.when(messageConverter.createToUserMessage(USER_DISCONNECTION, CERTAIN_LOGIN))
                .thenReturn(TO_USER_DISCONNECTION);
        Mockito.when(userSessionCache.getLoginSet()).thenReturn(ONLINE_USERS);
        Mockito.when(userSessionCache.getSession(CERTAIN_LOGIN_1)).thenReturn(login1Session);
        Mockito.when(userSessionCache.getSession(CERTAIN_LOGIN_3)).thenReturn(login3Session);
        Mockito.when(login1Session.getRemote()).thenReturn(login1RemoteEndpoint);
        Mockito.when(login3Session.getRemote()).thenReturn(login3RemoteEndpoint);

        cut.notifyUserDisconnected(CERTAIN_LOGIN);

        Mockito.verify(login1RemoteEndpoint, Mockito.times(1)).sendString(TO_USER_DISCONNECTION);
        Mockito.verify(login3RemoteEndpoint, Mockito.times(1)).sendString(TO_USER_DISCONNECTION);
    }
}
