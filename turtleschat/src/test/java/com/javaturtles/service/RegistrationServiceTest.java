package com.javaturtles.service;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.javaturtles.dao.IDbService;
import com.javaturtles.passwordencoder.PasswordEncoder;
import com.javaturtles.util.validation.Validation;
import com.javaturtles.dto.RegistrationDto;
import com.javaturtles.dto.ResponseDto;
import com.javaturtles.model.User;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;
import static com.javaturtles.testdata.service.RegistrationServiceTestData.*;

class RegistrationServiceTest {

    private final Gson gson = Mockito.mock(Gson.class);
    private final Validation registrationValidation = Mockito.mock(Validation.class);
    private final IDbService iDbService = Mockito.mock(IDbService.class);
    private final PasswordEncoder passwordEncoder = Mockito.mock(PasswordEncoder.class);

    RegistrationService cut = new RegistrationService(gson,
            registrationValidation, iDbService, passwordEncoder);

    static Arguments[] registerUserTestArgs() {
        return new Arguments[]{
                Arguments.arguments(CORRECT_BODY_1, CORRECT_REG_DTO_1, MSG_SUCCESS, HASHED_PASSWORD, USER_1, true, RESPONSE_OBJECT_CORRECT),
                Arguments.arguments(BODY_INCORRECT_LOGIN, INCORRECT_LOGIN_REG_DTO_1, MSG_INVALID_INCORRECT_LOGIN, null, null, false, RESPONSE_OBJECT_INCORRECT),
        };
    }

    @MethodSource("registerUserTestArgs")
    @ParameterizedTest
    void registerUserTest(String body, RegistrationDto registrationDTO,
                          String validateResponse,
                          String hashPassword, User user,
                          boolean add, ResponseDto expected) {

        Mockito.when(gson.fromJson(body, RegistrationDto.class)).thenReturn(registrationDTO);
        Mockito.when(registrationValidation.getValidateResponse(registrationDTO)).thenReturn(validateResponse);
        Mockito.when(passwordEncoder.getHash(registrationDTO.getPassword())).thenReturn(hashPassword);
        Mockito.when(iDbService.addUser(user)).thenReturn(add);
        ResponseDto actual = cut.registerUser(body);
        Assertions.assertEquals(expected, actual);

    }

    static Arguments[] registerUserExceptionTestArgs() {
        return new Arguments[]{
                Arguments.arguments(BODY_EXCEPTION, RESPONSE_OBJECT_EXCEPTION)
        };
    }

    @MethodSource("registerUserExceptionTestArgs")
    @ParameterizedTest
    void registerUserExceptionTest(String body, ResponseDto expected) {

        Mockito.when(gson.fromJson(body, RegistrationDto.class)).thenThrow(JsonSyntaxException.class);
        ResponseDto actual = cut.registerUser(body);
        Assertions.assertEquals(expected, actual);

    }
}