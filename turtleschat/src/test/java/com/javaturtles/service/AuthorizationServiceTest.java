package com.javaturtles.service;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.javaturtles.dto.AuthorizationDto;
import com.javaturtles.dto.RegistrationDto;
import com.javaturtles.dto.ResponseDto;
import com.javaturtles.util.jwt.JWTUtil;
import com.javaturtles.util.validation.Validation;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;

import javax.servlet.ServletException;
import java.io.IOException;

import static com.javaturtles.testdata.service.AuthorizationServiceTestData.*;

class AuthorizationServiceTest {

    Gson gson = Mockito.mock(Gson.class);
    JWTUtil jwtUtil = Mockito.mock(JWTUtil.class);
    Validation validation = Mockito.mock(Validation.class);

    AuthorizationService cut = new AuthorizationService(gson, validation, jwtUtil);

    static Arguments[] authorizeUserTestArgs() {
        return new Arguments[]{
                Arguments.arguments(BODY_1, AUTH_DTO_1_SUCCESS, MSG_SUCCESS, TT_MILLIS, JWT_TOKEN, RESPONSE_DTO_SUCCESS),
                Arguments.arguments(BODY_2, AUTH_DTO_2_WRONG, MSG_INVALID_INCORRECT_LOGIN, TT_MILLIS, null, RESPONSE_DTO_INVALID_LOGIN),
        };
    }

    @ParameterizedTest
    @MethodSource("authorizeUserTestArgs")
    void authorizeUserTest(String body, AuthorizationDto authDTO, String validateResponse,
                           int ttMillis, String jwtToken, ResponseDto expected) throws ServletException, IOException {

        Mockito.when(gson.fromJson(body, AuthorizationDto.class)).thenReturn(authDTO);
        Mockito.when(validation.getValidateResponse(authDTO)).thenReturn(validateResponse);
        Mockito.when(jwtUtil.createJWT(authDTO.getLogin(), ttMillis)).thenReturn(jwtToken);

        ResponseDto actual = cut.authorizeUser(body);
        Assertions.assertEquals(expected, actual);
    }

    static Arguments[] authorizeUserExceptionTestArgs() {
        return new Arguments[]{
                Arguments.arguments(BODY_3, RESPONSE_DTO_INCORRECT_DATA),
                Arguments.arguments(null, RESPONSE_DTO_INCORRECT_DATA)
        };
    }

    @MethodSource("authorizeUserExceptionTestArgs")
    @ParameterizedTest
    void authorizeUserExceptionTest(String body, ResponseDto expected) {

        Mockito.when(gson.fromJson(body, RegistrationDto.class)).thenThrow(JsonSyntaxException.class);
        ResponseDto actual = cut.authorizeUser(body);
        Assertions.assertEquals(expected, actual);

    }

}